def parse(query: str) -> dict:
    parsed_dictionary = {}

# Checking, if we have "?" separator in url, and if yes, splitting it
    if "?" in query:
        parameters = query.split("?")[1].split("&")

# Looping through the list and if it is a "=" separator in list element, splitting element.
        for item in parameters:
            if "=" in item:
                dict_item = item.split("=")
                parsed_dictionary.update({dict_item[0]: dict_item[1]})  # Updating dictionary
    return parsed_dictionary


if __name__ == '__main__':
    assert parse('https://example.com/path/to/page?name=ferret&color=purple') == {'name': 'ferret', 'color': 'purple'}
    assert parse('https://example.com/path/to/page?name=ferret&color=purple&') == {'name': 'ferret', 'color': 'purple'}
    assert parse('http://example.com/') == {}
    assert parse('http://example.com/?') == {}
    assert parse('http://example.com/?name=John') == {'name': 'John'}

# Additional tests
    assert parse('http://example.com/?name=') == {'name': ''}
    assert parse('http://example.com/?name') == {}
    assert parse('http://example.com/?month=May&day=20') == {'month': 'May', 'day': '20'}
    assert parse('http://example.com/?month=May&day=20&&') == {'month': 'May', 'day': '20'}
    assert parse('http://example.com/?year=2023&month=October?day=4') == {'year': '2023', 'month': 'October'}


def parse_cookie(query: str) -> dict:
    parsed_dictionary = {}

# Checking, if we have ";" separator in query, and if yes, splitting it
    if ";" in query:
        params = query.split(";")

# Looping through the list and if it is a "=" separator in list element, splitting element.
        for item in params:
            if "=" in item:
                dict_item = item.split("=", maxsplit=1)  # Use "split" only one time in each element
                parsed_dictionary.update({dict_item[0]: dict_item[1]})
    return parsed_dictionary


if __name__ == '__main__':
    assert parse_cookie('name=John;') == {'name': 'John'}
    assert parse_cookie('') == {}
    assert parse_cookie('name=John;age=28;') == {'name': 'John', 'age': '28'}
    assert parse_cookie('name=John=User;age=28;') == {'name': 'John=User', 'age': '28'}

# Additional tests
    assert parse_cookie('name=John===Don;') == {'name': 'John===Don'}
    assert parse_cookie('film:movie') == {}
    assert parse_cookie('name=Dog;;;breed=Corgi') == {'name': 'Dog', 'breed': 'Corgi'}
    assert parse_cookie('name=Dog;breed') == {'name': 'Dog'}
    assert parse_cookie('name=Dog;breed=') == {'name': 'Dog', 'breed': ''}
